<?php

namespace HolmBank\Payments\Api;

use HolmBank\Payments\Api\Data\HolmbankProductInterface;

interface HolmbankProductRepositoryInterface
{

    public function save(HolmbankProductInterface $order): void;

    public function getByProductType($productType): HolmbankProductInterface;
}
