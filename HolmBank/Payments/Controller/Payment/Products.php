<?php

namespace HolmBank\Payments\Controller\Payment;

use HolmBank\Payments\Model\Config\Source\Products as ProductSettings;
use Magento\Checkout\Controller\Action;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Products extends Action
{
    private JsonFactory $resultJsonFactory;

    private ProductSettings $products;

    public function __construct(
        Context $context,
        Session       $customerSession,
        CustomerRepositoryInterface           $customerRepository,
        AccountManagementInterface            $accountManagement,
        JsonFactory                           $resultJsonFactory,
        ProductSettings                       $products
    )
    {
        parent::__construct(
            $context,
            $customerSession,
            $customerRepository,
            $accountManagement
        );
        $this->resultJsonFactory = $resultJsonFactory;
        $this->products = $products;
    }

    public function execute()
    {
        $availableProducts = $this->products->getCheckedValues();
        $result = $this->resultJsonFactory->create();
        $result->setHeader("Content-Type", "application/json");
        return $result->setData($availableProducts);
    }
}
