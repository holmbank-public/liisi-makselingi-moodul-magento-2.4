<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class HolmBankProduct extends AbstractDb
{
    /**
     * @var string
     */
    protected string $_eventPrefix = 'holmbank_products_resource_model';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('holmbank_products', 'entity_id');
        $this->_useIsObjectNew = true;
    }
}
