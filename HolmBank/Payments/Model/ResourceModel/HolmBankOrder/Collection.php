<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model\ResourceModel\HolmBankOrder;

use HolmBank\Payments\Model\HolmBankOrder as Model;
use HolmBank\Payments\Model\ResourceModel\HolmBankOrder as ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'holmbank_order_collection';

    /**
     * Initialize collection model.
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}
