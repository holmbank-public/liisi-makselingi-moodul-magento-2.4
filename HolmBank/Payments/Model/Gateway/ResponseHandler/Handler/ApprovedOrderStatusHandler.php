<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model\Gateway\ResponseHandler\Handler;

use HolmBank\Payments\Api\Data\HolmbankOrderInterface;
use HolmBank\Payments\Model\HolmbankOrderRepository;
use HolmBank\Payments\Model\Payment;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\InvoiceManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Config as OrderConfig;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Sales\Model\Order\Invoice;

/**
 * Approved payment status response handler
 */
class ApprovedOrderStatusHandler implements ResponseHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private OrderRepositoryInterface $orderRepository;

    /**
     * @var InvoiceManagementInterface
     */
    private InvoiceManagementInterface $invoiceManagement;

    /**
     * @var HolmbankOrderRepository
     */
    private HolmbankOrderRepository $holmbankOrderRepository;

    /**
     * @var OrderConfig
     */
    private OrderConfig $orderConfig;

    /**
     * @var OrderSender
     */
    private OrderSender $orderSender;

    /**
     * @param OrderRepositoryInterface $orderRepository
     * @param InvoiceManagementInterface $invoiceManagement
     * @param HolmbankOrderRepository $holmbankOrderRepository
     * @param OrderConfig $orderConfig
     * @param OrderSender $orderSender
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        InvoiceManagementInterface $invoiceManagement,
        HolmbankOrderRepository $holmbankOrderRepository,
        OrderConfig $orderConfig,
        OrderSender $orderSender
    ) {
        $this->orderRepository = $orderRepository;
        $this->invoiceManagement = $invoiceManagement;
        $this->holmbankOrderRepository = $holmbankOrderRepository;
        $this->orderConfig = $orderConfig;
        $this->orderSender = $orderSender;
    }

    /**
     * @inheritdoc
     */
    public function handle(OrderInterface $order, HolmbankOrderInterface $holmbankOrder): void
    {
        $holmbankOrder->setOrderStatus(Payment::APPROVED_STATUS);
        $this->holmbankOrderRepository->save($holmbankOrder);

        $order->setState(Order::STATE_PROCESSING);
        $order->setStatus($this->orderConfig->getStateDefaultStatus($order->getState()));
        $order->addCommentToStatusHistory(__('Received approved status from payment gateway.'));

        $invoice = $this->prepareInvoice($order);
        if (null === $invoice) {
            $order->addCommentToStatusHistory(__('Order cannot be invoiced.'));
        }

        $this->orderRepository->save($order);

        if (false === (bool)$order->getEmailSent()) {
            $this->orderSender->send($order);
        }
    }

    /**
     * Register order invoice.
     *
     * @param OrderInterface $order
     *
     * @return Invoice|null
     *
     * @throws LocalizedException
     */
    private function prepareInvoice(OrderInterface $order): ?Invoice
    {
        if (!$order->canInvoice()) {
            return null;
        }

        /** @var Invoice $invoice */
        $invoice = $this->invoiceManagement->prepareInvoice($order);
        $invoice->register();

        $order->addRelatedObject($invoice);

        return $invoice;
    }
}
