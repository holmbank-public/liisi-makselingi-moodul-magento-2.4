<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model\Config\Source;

class Checkbox
{

    public static function toOptionArray()
    {
        return [
            ['value' => 1, 'label' => __('Enable')],
            ['value' => 0, 'label' => __('Disable')]
        ];
    }
}
