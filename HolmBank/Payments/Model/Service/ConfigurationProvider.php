<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model\Service;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Service configuration provider.
 */
class ConfigurationProvider
{
    /**
     * Configuration paths.
     */
    private const XML_PATH_PRODUCTS = 'payment/holm_partner/products';

    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Retrieve active products.
     *
     * @return string[]
     */
    public function getActiveProducts(): array
    {
        $products = (string)$this->scopeConfig->getValue(self::XML_PATH_PRODUCTS, ScopeInterface::SCOPE_STORES);

        return \array_filter(\explode(',', $products));
    }
}
