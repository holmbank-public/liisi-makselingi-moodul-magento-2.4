define([
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ], function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'holm_partner',
                component: 'HolmBank_Payments/js/view/payment/method-renderer/holmbank-payments'
            }
        );
        return Component.extend({});
    }
);
