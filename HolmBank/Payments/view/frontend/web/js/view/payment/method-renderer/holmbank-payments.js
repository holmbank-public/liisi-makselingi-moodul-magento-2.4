define([
    'ko',
    'jquery',
    'Magento_Checkout/js/view/payment/default',
    'mage/url',
    'Magento_Customer/js/customer-data',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Checkout/js/model/full-screen-loader'
], function (
    ko,
    $,
    Component,
    url,
    customerData,
    errorProcessor,
    fullScreenLoader
) {
    'use strict';

    return Component.extend({
        redirectAfterPlaceOrder: false,
        selectedPaymentMethodType: ko.observable(null),

        defaults: {
            template: 'HolmBank_Payments/payment/holmbank-payments'
        },

        /**
         * @inheritdoc
         */
        initialize: function () {
            this._super();
            this.paymentMethods(window.checkoutConfig.payment.holm_bank);
        },

        /**
         * @inheritdoc
         */
        initObservable: function() {
            this._super().observe(['paymentMethods']);

            return this;
        },

        /**
         * @inheritdoc
         */
        selectPaymentMethod: function (selectedMethod) {
            this.selectedPaymentMethodType(selectedMethod.type);

            return this._super();
        },

        /**
         * @inheritdoc
         */
        getData: function () {
            return {
                'method': this.item.method,
                'po_number': null,
                'additional_data': {
                    method_type: this.selectedPaymentMethodType(),
                }
            };
        },

        /**
         * After place order action.
         */
        afterPlaceOrder: function () {
            var redirectUrl = url.build('holmbank/payment/create');
            $.ajax({
                url: redirectUrl,
                type: 'GET',
                async: false,
            }).done(function (response) {
                customerData.invalidate(['cart']);
                window.location.href = response.redirectUrl;
            }).fail(function (response) {
                errorProcessor.process(response, this.messageContainer);
            }).always(function () {
                fullScreenLoader.stopLoader();
            });
        }
    });
});


