<?php

declare(strict_types=1);

namespace HolmBank\Payments\Block\Payment;

use HolmBank\Payments\Model\HolmbankProductRepository;
use Magento\Framework\View\Element\Template\Context;
use Magento\Payment\Block\Info as PaymentInfo;

/**
 * @inheritdoc
 */
class Info extends PaymentInfo
{
    /**
     * @var HolmbankProductRepository
     */
    private HolmbankProductRepository $holmbankProductRepository;

    /**
     * @param Context $context
     * @param HolmbankProductRepository $holmbankProductRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        HolmbankProductRepository $holmbankProductRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->holmbankProductRepository = $holmbankProductRepository;
    }

    /**
     * @inheritdoc
     */
    protected function _prepareSpecificInformation($transport = null)
    {
        $information = parent::_prepareSpecificInformation($transport);
        $methodType = $this->getInfo()->getAdditionalInformation('method_type');

        if (!$methodType) {
            return $information;
        }

        try {
            $product = $this->holmbankProductRepository->getByProductType($methodType);
        } catch (\Throwable $exception) {
            return $information;
        }

        $information->addData([(string)__('Product') => $product->getHolmBankProductName()]);

        return $information;
    }
}
