<?php

declare(strict_types=1);

namespace HolmBank\Payments\Observer;

use Magento\Framework\Event\Observer;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Quote\Api\Data\PaymentInterface;

/**
 * Observer responsible for assigning additional payment data.
 */
class AssignHolmPartnerAdditionalPaymentData extends AbstractDataAssignObserver
{
    /**
     * @inheritdoc
     */
    public function execute(Observer $observer): void
    {
        $dataObject = $this->readDataArgument($observer);
        $paymentInfo = $this->readPaymentModelArgument($observer);
        $additionalData = $dataObject->getData(PaymentInterface::KEY_ADDITIONAL_DATA);

        if (false === \is_array($additionalData)) {
            return;
        }

        $paymentInfo->setAdditionalInformation('method_type', $additionalData['method_type'] ?? '');
    }
}
